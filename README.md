<h1 align="center">Curriculum Vitae with Laravel 9 & Vue3 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-0.0.1--alpha-blue.svg?cacheSeconds=2592000" />
  <img src="https://img.shields.io/badge/npm-%3E%3D8.5.0-blue.svg" />
  <img src="https://img.shields.io/badge/node-%3E%3D16.15.0-blue.svg" />
  <a href="#" target="_blank">
    <img alt="License: MIT" src="https://img.shields.io/badge/License-MIT-yellow.svg" />
  </a>
</p>

> My personnal developer & project manager Curriculum Vitae, with Laravel 9 & Vue3

## Prerequisites
- npm >=8.5.0
- node >=16.15.0

### 🏠 [Homepage](https://gitlab.com/SmashScharrer-Dev/curriculum-vitae-laravel-9-vue3)

## Install
```sh
Be soon...
```

## Author
👤 **SmashScharrer-Dev**

## 🤝 Contributing
Contributions, issues and feature requests are welcome!<br />Feel free to check [issues page](https://gitlab.com/SmashScharrer-Dev/curriculum-vitae-laravel-9-vue3/-/issues). You can also take a look at the [contributing guide](https://gitlab.com/SmashScharrer-Dev/curriculum-vitae-laravel-9-vue3/blob/master/CONTRIBUTING.md).

## Show your support
Give a ⭐️ if this project helped you!

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_