import './bootstrap';
import 'boxicons/css/boxicons.min.css';
import { createApp } from 'vue';
import App from '../vue/app.vue';

createApp(App).mount("#app");
